import re
from abc import abstractmethod
from logging import Logger
from typing import Set, Dict, Callable, Any, Optional
from urllib.parse import urlparse

from bs4 import BeautifulSoup

from scrapper.scrapper import IDataExtractor, UriReturnsEmptyBody


class IResultRepository:
    @abstractmethod
    async def save(self, data):
        pass


class Bs4DictDataExtractor(IDataExtractor):
    _match_js_link = re.compile("^.*javascript:[a-zA-Z0-9_]+[ ]*(\(.*\))?[ ]*$")
    _match_mailto_link = re.compile("^.*mailto:[^@]+@[^@]+\.[^@]+$")

    def __init__(self,
                 result_repository: IResultRepository,
                 extractors: Dict[str, Callable[[str, BeautifulSoup], Any]],
                 data_uri_matcher: Callable[[str], bool],
                 logger: Logger,
                 next_uri_matcher: Callable[[str], bool] = None,
                 selector: Optional[str] = None,
                 ):
        self._extractors = extractors or {}
        self._result_repository = result_repository
        self._selector: str = selector or 'a[href]'
        self._next_uri_matcher = next_uri_matcher or (lambda x: True)
        self._data_uri_matcher = data_uri_matcher
        self._logger = logger.getChild("bs4_dict_data_extractor")

    async def _extract_dict(self, uri: str, soup: BeautifulSoup):
        data = {}
        try:
            for el, extract in self._extractors.items():
                data[el] = extract(uri, soup)
            return data
        except Exception as e:
            self._logger.info(f"error {str(e)} on extracting data of uri {uri}")
            raise e

    @staticmethod
    def _should_follow_uri(uri) -> bool:
        return not bool(Bs4DictDataExtractor._match_js_link.match(uri)) and \
               not bool(Bs4DictDataExtractor._match_mailto_link.match(uri))

    async def _extract_uris(self, uri: str, soup: BeautifulSoup):
        uris = set()
        for link in soup.select(self._selector):
            next_uri_candidate = link.attrs['href']
            if _is_absolute(next_uri_candidate):
                if not _is_same_domain(uri, next_uri_candidate):
                    continue
            else:
                parsed_uri = urlparse(uri)
                next_uri_candidate = urlparse(f"{parsed_uri.scheme}://{parsed_uri.netloc}/{next_uri_candidate.strip('/')}").geturl()

            if self._next_uri_matcher(next_uri_candidate) and Bs4DictDataExtractor._should_follow_uri(next_uri_candidate):
                uris.add(next_uri_candidate)
        return uris

    async def extract(self, uri: str, document: str) -> Set[str]:
        if document is None:
            raise UriReturnsEmptyBody(uri)
        soup = BeautifulSoup(document, 'html.parser')
        next_uris = await self._extract_uris(uri, soup)

        if self._data_uri_matcher(uri):
            data = await self._extract_dict(uri, soup)
            await self._result_repository.save(data)

        return next_uris


def _is_absolute(url):
    return bool(urlparse(url).netloc)


def _is_same_domain(uri_base, uri):
    uri_base = urlparse(uri_base)
    uri = urlparse(uri)
    return uri.netloc == uri_base.netloc and uri.scheme == uri_base.scheme
