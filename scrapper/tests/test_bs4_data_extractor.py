import asyncio
import re
import unittest
from unittest.mock import MagicMock, call

from scrapper.bs4_data_extractor import Bs4DictDataExtractor

_TEMPLATE_1 = """
<section class='contas"'>
    <a href='https://goo.appl.micro'>
            Vaii
    </a>
    <a href='https://abacate.co.us'>
            Vaii
    </a>
    <a href='https://banaaa.com.br'>
            Vaii
    </a>
    <a href='https://banana.com.br/sair'>
            Vaii
    </a>
    <a href='https://banana.com.br/conta/1'>
            Conta 2
    </a>
    <a href='https://banana.com.br/conta/2'>
            Conta 3
    </a>
    <a href='https://banana.com.br/conta/javascript:alert(0)'>
            Conta 3
    </a>
    <a href='https://banana.com.br/conta/javascript:run("923", "3234")'>
            Conta 3
    </a>
    <a href='https://banana.com.br/conta/javascript:roda(12, "3234", 15.6, {"key": "value"})'>
            Conta 3
    </a>
    <a href='/conta/23'>
            Conta 3
    </a>
    <a href='https://banana.com.br/mailto:site.sac@abacae.co.br'>
            Conta 3
    </a>
</section>
<section>
    <div class="account-name">Conta 1</div>
    <div class='total-amount'>R$ 32,00</div>
</section>
    

"""


class TestBs4DataExtractor(unittest.TestCase):
    def setUp(self):
        self.repository = MagicMock()

        async def save(*args, **kwargs):
            return call(args=args, kwargs=kwargs)

        self.repository.save.side_effect = save
        data_uri_matcher = re.compile("^.*/conta/[0-9]+/?$")
        self.extractor = Bs4DictDataExtractor(
            logger=MagicMock(),
            result_repository=self.repository,
            extractors={
                "account_name": lambda _, soup: soup.select("div.account-name")[0].text,
                "amount": lambda _, soup: soup.select("div.total-amount")[0].text
            },
            data_uri_matcher=lambda s: bool(data_uri_matcher.match(s)),
        )

    def test_data_extractor(self):
        async def run():
            next_uris = await self.extractor.extract("https://banana.com.br/conta/0", document=_TEMPLATE_1)
            expected = {'https://banana.com.br/conta/2',
                        'https://banana.com.br/conta/1',
                        'https://banana.com.br/conta/23',
                        'https://banana.com.br/sair'}
            self.assertEqual(expected, next_uris)
            self.repository.save.assert_has_calls([call({"account_name": "Conta 1", "amount": "R$ 32,00"})])

        asyncio.get_event_loop().run_until_complete(run())

    def test_must_ignore_data_uris_that_no_match(self):
        async def run():
            next_uris = await self.extractor.extract("https://banana.com.br/contaas/0", document=_TEMPLATE_1)
            expected = {'https://banana.com.br/conta/2',
                        'https://banana.com.br/conta/1',
                        'https://banana.com.br/conta/23',
                        'https://banana.com.br/sair'}
            self.assertEqual(expected, next_uris)
            self.repository.save.assert_not_called()

        asyncio.get_event_loop().run_until_complete(run())
