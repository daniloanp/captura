import asyncio
import unittest
from unittest.mock import MagicMock

from scrapper import Scrapper


class TestScapper(unittest.TestCase):
    urls = {
        "ehey",
        "hou"
    }
    templates = {
        "http://test.com.br": """
            <a href="https://banana.com.br/dddd">banana</a>
            <a href="http://banana.com.br/produtores">banana</a>
           <a href="http://banana.com.br/tipos">banana</a>
           <a href="/da-terra">da-terra</a>
           <a href="/dagua">da-terra</a>
           <a href="/prata">da-terra</a>
           <a href="http://clapton.co">Eric clapton</a>
           """
    }

    @staticmethod
    async def _render(uri):
        return TestScapper.templates[uri]

    async def _scrap(self, uri, document):
        self.assertEqual(await TestScapper._render(uri), document)
        return TestScapper.urls

    def test_url_matching(self):
        renderer = MagicMock()

        renderer.render.side_effect = TestScapper._render

        data_extractor = MagicMock()

        data_extractor.extract.side_effect = self._scrap
        scrapper = Scrapper(
            renderer=renderer,
            data_extractor=data_extractor,
        )

        async def run():
            with self.assertRaises(KeyError):
                await scrapper.scrap("http://banana.com.br")
            uris = await scrapper.scrap("http://test.com.br")
            self.assertEqual(TestScapper.urls, uris)

        asyncio.get_event_loop().run_until_complete(run())
