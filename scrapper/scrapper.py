from abc import abstractmethod
from typing import Set


class UriReturnsEmptyBody(Exception):
    def __init__(self, uri):
        self.uri = uri

    def __str__(self):
        return f"{self.uri} could not be scrapped.\tCause: empty body"


class UriCouldNotBeRendered(Exception):
    def __init__(self, uri, cause=None):
        self.uri = uri
        self.cause = cause

    def __str__(self):
        return f"{self.uri} could not be rendered\tCause: {str(self.cause) or 'unknown'}"


class IDataExtractor:
    @abstractmethod
    async def extract(self, uri: str, document: str) -> Set[str]:
        pass


class IRenderer:
    @abstractmethod
    async def render(self, uri) -> str:
        pass


class Scrapper:
    def __init__(self,
                 renderer: IRenderer,
                 data_extractor: IDataExtractor):
        self.data_extractor = data_extractor
        self.renderer = renderer

    async def scrap(self, uri) -> Set[str]:
        document = await self.renderer.render(uri)
        uris_found = await self.data_extractor.extract(uri, document)
        return uris_found
