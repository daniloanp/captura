# Desafio Captura

Trata-se de uma ferramenta de linha de comando para capturar dados dos produtos de um [website](https://epocacosmeticos.com.br)


### Como rodar
Para rodar o script é necessário ter o python 3.6 instalado e instalar os pacotes listados no `requirements.txt`.

Caso seu sistema operacional não tenha um pacote para essa versão, recomendo usar o [pyenv](https://github.com/pyenv/pyenv) para instalar.

```
$ python -m venv ${path} #troque ${path} pelo caminho desejado 
$ source ${path}/bin/activate
$ pip install -r requirements.txt
```


Agora basta rodar
```
$ python products_from_epocacosmeticos.py --output output.csv
```

Para verificar por opções adicionais:
```
$ python products_from_epocacosmeticos.py --help
```

## Como rodar os tests

```
$ python -m unittest discover
```

### Quero rodar de dentro de um docker

Primeiramente você precisa construir a imagem. Vá para a raiz do repositório e rode os comandos a seguir.
```bash
$ docker build -t captura .
```

Uma vez que este script escreve no sistema de arquivos, precisaremos plugar um volume no docker para obter o resultado. Podemos escrever
```
$ docker run -v $PWD:/out captura python products_from_epocacosmeticos.py --output /out/output.csv
```

## Autores

* **Danilo Pereira**  - [GitHub](https://github.com/dnp1)

