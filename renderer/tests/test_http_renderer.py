import asyncio
import unittest
from unittest.mock import MagicMock

from aiohttp import web
from aiohttp.test_utils import unused_port

from renderer import HttpRenderer
from scrapper.scrapper import UriCouldNotBeRendered


class TestHttpRenderer(unittest.TestCase):
    @staticmethod
    async def _test(_):
        return web.Response(
            body="test"
        )

    @staticmethod
    async def _test_sub_path(_):
        return web.Response(
            body="subpath"
        )

    def setUp(self):
        self.loop = asyncio.get_event_loop()
        self.app = web.Application()
        self.app.add_routes(
            [web.get('/test', TestHttpRenderer._test),
             web.get('/test/sub-path', TestHttpRenderer._test_sub_path)]
        )
        self.port = unused_port()

        async def init_server():
            self.runner = web.AppRunner(self.app)
            await self.runner.setup()
            site = web.TCPSite(self.runner, '127.0.0.1', self.port)
            await site.start()

        self.loop.run_until_complete(init_server())

    def tearDown(self):
        self.loop.run_until_complete(self.runner.cleanup())

    def test_http_renderer(self):
        renderer = HttpRenderer(logger=MagicMock())

        async def render():
            with self.assertRaises(UriCouldNotBeRendered):
                await renderer.render(f"http://127.0.0.1:{self.port}/404")
            output = await renderer.render(f"http://127.0.0.1:{self.port}/test")
            self.assertIn("test", output)
            output = await renderer.render(f"http://127.0.0.1:{self.port}/test/sub-path")
            self.assertIn("subpath", output)

        self.loop.run_until_complete(render())
