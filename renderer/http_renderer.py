import asyncio
import math

import yarl
from logging import Logger

import aiohttp

from scrapper.scrapper import UriCouldNotBeRendered


class _UnexpectedStatus(Exception):
    def __init__(self, uri, status):
        self.status = status
        self.uri = uri

    def __str__(self):
        return f"unexpected status {self.status} when accessing uri {self.uri}"


class HttpRenderer:
    def __init__(self, logger: Logger, max_retries=3):
        self._logger: Logger = logger.getChild("http_renderer")
        self.max_retries = max_retries

    async def render(self, uri, level=0) -> str:
        uri = yarl.URL(uri, encoded=False)
        try:
            if level:
                await asyncio.sleep(math.factorial(level))
                self._logger.info(f"retrying request to {uri} for {level}th time")
            async with aiohttp.ClientSession() as session:
                async with session.get(uri) as resp:
                    if resp.status != 200:
                        raise _UnexpectedStatus(uri, resp.status)
                    body = await resp.text()
                    return body
        except _UnexpectedStatus as e:
            raise UriCouldNotBeRendered(uri, cause=e)
        except (aiohttp.ClientConnectorError, asyncio.TimeoutError) as e:
            if level < self.max_retries:
                self._logger.error(f"request to {uri} has failed due to {str(e)}... retrying")
                return await self.render(uri, level + 1)
            else:
                raise UriCouldNotBeRendered(uri, cause="too many retries")
