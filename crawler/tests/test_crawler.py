import asyncio
import unittest
from unittest.mock import MagicMock

from crawler.crawler import Crawler


class TestCrawler(unittest.TestCase):
    def test_duplicated_uris_are_not_accessed(self):
        scrapper = MagicMock()
        uris = {
            "http://bit.co": {'http://bit.co', 'http://bit.co/a', 'http://bit.co/b', 'http://bit.co/c'},
            'http://bit.co/a': {'http://bit.co', 'http://bit.co/a', 'http://bit.co/a/1',
                                'http://bit.co/a/2', 'http://bit.co/b/a'},
            'http://bit.co/b': {'http://bit.co', 'http://bit.co/b/112', 'http://bit.co/b/a',
                                'http://bit.co/b/11fa34-48q'},
            'http://bit.co/c': {'http://bit.co/b/11fa34-48q', 'http://bit.co/c/1', 'http://bit.co/c/2'}
        }
        visits = {}

        async def scrap(uri):
            visits[uri] = visits.get(uri, 0) + 1
            next_uris = uris.get(uri, set())
            return next_uris

        scrapper.scrap.side_effect = scrap
        crawler = Crawler(scrapper, logger=MagicMock(), max_concurrent_paths=10, wait_secs=.01)

        async def case():
            await crawler.crawl("http://bit.co")

        asyncio.get_event_loop().run_until_complete(case())
        for key, visit_count in visits.items():
            self.assertEqual(1, visit_count)
