import asyncio
from abc import abstractmethod
from logging import Logger
from typing import Set


class IScrapper:
    @abstractmethod
    async def scrap(self, url) -> Set[str]:
        pass


class Crawler:
    def __init__(self, scrapper: IScrapper, logger: Logger, max_concurrent_paths, wait_secs=.5):
        self._scrapper = scrapper
        self._logger = logger.getChild("crawler")
        self._max_concurrent_paths = max_concurrent_paths
        self._wait_secs = wait_secs

    async def crawl(self, uri: str):
        await _Crawling(self._scrapper, self._logger, self._max_concurrent_paths, wait_secs=self._wait_secs).crawl(uri)


class _Crawling:
    def __init__(self, scrapper: IScrapper, logger: Logger, max_concurrent_paths, wait_secs):
        self._scrapper = scrapper
        self._visited_urls: Set[str] = set()
        self._logger = logger
        self._max_concurrent_paths = max_concurrent_paths
        self._wait_secs = wait_secs

    async def _scrap(self, uri) -> Set[str]:
        return await self._scrapper.scrap(uri)

    async def crawl(self, uri):
        uris_to_found = {uri} - self._visited_urls
        pending = []
        while uris_to_found:
            next_uris = set()
            for uri in uris_to_found:
                if len(pending) >= self._max_concurrent_paths:
                    break
                self._visited_urls.add(uri)
                pending.append(self._scrap(uri))

            wait_group = asyncio.wait(pending, return_when=asyncio.FIRST_COMPLETED)
            await asyncio.sleep(self._wait_secs)
            done, pending_set = await wait_group
            pending = list(pending_set)
            for task in done:
                exc = task.exception()
                if exc:
                    self._logger.critical(exc)
                else:
                    result_uris = task.result()
                    next_uris = next_uris.union(result_uris)

            self._logger.info(f"{len(self._visited_urls)} has been visited")
            uris_to_found = uris_to_found.union(next_uris) - self._visited_urls
