#### Respostas

- Agora você tem de capturar dados de outros 100 sites. Quais seriam suas estratégias para escalar a aplicação?
    * A aplicação poderia carregar um arquivo de configuração (json, toml, yaml) as definições de crawling e scrapper.
    * Por exemplo: poderiamos ter um método que constroi o **extrator** a partir de um dicionário e receberiamos como primeiro argumento da linha de comando o *site* a ser percorrido.
    * Claro, se a alteração de parametros for frequente o ideal seria armazenar esses dados em um banco de dados e oferecer uma api de definição de extrator e de execução
- Alguns sites carregam o preço através de JavaScript. Como faria para capturar esse valor.
    * Poderia trocar meu *http_render* por um render que usasse Selenium ou phantomjs. 
    * Utilizaria um evento js de alteração no DOM para verificar se a página já contém os campos desejados e só então retornaria o HTML para a aplicação.
    
- Alguns sites podem bloquear a captura por interpretar seus acessos como um ataque DDOS. Como lidaria com essa situação?
    * A primeira abordagem seria utilizar uma lista de *proxies*.
    * Se o site utilizar captchas como proteção, poderiamos utilizar um serviço que resolve captchas.
    * No entanto, vejo como ideal conversar com o administrador do site e requisitar um *Token* com autorização para efetuar o *crawling*
    
- Um cliente liga reclamando que está fazendo muitos acessos ao seu site e aumentando seus custos com infra. Como resolveria esse problema?
    * Poderia pedir para o cliente gerar um espelho estático do site dele 
    * Ou poderia também rodar de dentro da rede dele, caso os custos sejam de rede;
    * Não consigo vizualizar muitas saídas para isso. Acho, no entanto, improvavel que os custos de infra sejam resposabilidade do crawler. Iria conversar com o cliente para entender o que está acontecendo.
    
    
    


