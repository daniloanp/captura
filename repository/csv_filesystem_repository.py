from csv import DictWriter
from typing import List


class CsvFileSystemRepository:
    def __init__(self, filename: str, fields: List[str], open_func=open, buffer_size=65536):
        self.filename = filename
        self.file = None
        self.writer: DictWriter = None
        self.fields = fields
        self._open = open_func
        self._buffer_size = buffer_size

    async def __aenter__(self):
        self.file = self._open(self.filename, "w+", self._buffer_size)
        self.writer = DictWriter(self.file, fieldnames=self.fields)
        self.writer.writeheader()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        self.file.flush()
        self.file.close()

    async def save(self, data):
        self.writer.writerow(data)
