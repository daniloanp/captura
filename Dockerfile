FROM python:3.6.5
WORKDIR /app
RUN mkdir /out
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .