import argparse
import asyncio
import logging
import re

from crawler import Crawler
from renderer import HttpRenderer
from repository import CsvFileSystemRepository
from scrapper.bs4_data_extractor import Bs4DictDataExtractor
from scrapper.scrapper import Scrapper


def create_epoca_costmeticos_extractor(repository, logger):
    _data_uri_matcher = re.compile("^.*epocacosmeticos\.com\.br/.*/p/?$")

    return Bs4DictDataExtractor(
        logger=logger,
        result_repository=repository,
        extractors={
            "título_da_página": lambda _, soup: soup.title.text,
            "nome": lambda _, soup: soup.select("div.productName")[0].text,
            "uri": lambda uri, _: uri,
        },
        data_uri_matcher=lambda s: bool(_data_uri_matcher.match(s)),
    )


async def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--url',
                        dest='url',
                        type=str,
                        help='The url to crawl.',
                        default="https://www.epocacosmeticos.com.br/",
                        required=False)
    parser.add_argument('--output',
                        dest='output',
                        type=str,
                        help='The file output',
                        required=True)
    parser.add_argument('--log-filename',
                        dest='log_filename',
                        type=str,
                        help='A file logger',
                        required=False)
    parser.add_argument('--log-level',
                        dest='log_level',
                        type=str,
                        help='allow you choose log level',
                        required=False,
                        default='INFO',
                        choices=['CRITICAL', 'FATAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG'])
    parser.add_argument('--max-simultaneous-requests',
                        dest='max_simultaneous_requests',
                        type=int,
                        help="allow you choose how many simultaneous requests de crawler can perform",
                        default=100,
                        required=False)

    parser.add_argument('--heartbeat',
                        dest='heartbeat',
                        type=float,
                        help='allow you choose how much time a group of requests '
                             'will be waited before perform another group',
                        default=.2,
                        required=False)

    args = parser.parse_args()
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=args.log_level,
                        filename=args.log_filename)
    logger = logging.getLogger(name="main")

    async with CsvFileSystemRepository(args.output, ["nome", "uri", "título_da_página"]) as repository:
        extractor = create_epoca_costmeticos_extractor(repository, logger)
        renderer = HttpRenderer(logger=logger)
        scrapper = Scrapper(data_extractor=extractor, renderer=renderer)
        crawler = Crawler(scrapper=scrapper,
                          logger=logger,
                          max_concurrent_paths=args.max_simultaneous_requests,
                          wait_secs=args.heartbeat)
        logger.info("Beginning crawling:")
        await crawler.crawl(args.url)
        logger.info("Crawling has ended with success")

    logging.shutdown()


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
